import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent {
  dataList= [];
  constructor(private route: ActivatedRoute, private weatherService: WeatherService) {
    weatherService.getFiveDayWeather(this.route.snapshot.params['cityData']).subscribe((data:any) => {
      data.list.forEach(element => {
          if(element.dt_txt.includes('9:00:00')){
            this.dataList.push(element)
          }
      });
      console.log(this.dataList);
    }, error => {
      console.log(error);
    })

  }

}
