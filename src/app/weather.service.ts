import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) {

  }

  getCurrentWeather(endpointValue) {
    return this.http.get('http://api.openweathermap.org/data/2.5/weather?q=' + endpointValue + '&appid=3d8b309701a13f65b660fa2c64cdc517')
  }


  getFiveDayWeather(endpointValue) {
    return this.http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + endpointValue + '&appid=3d8b309701a13f65b660fa2c64cdc517')
  }

}
