import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  europeCities = [{ city: 'London', value: 'Rome,italy' }
  , { city: 'Paris', value: 'Paris,France' },
{ city: 'Berlin', value: 'Berlin,Germany' },
{ city: 'London', value: 'London,UK' }
  , { city: 'Spain', value: 'Madrid,Spain' }
];
constructor(private router: Router) {

}

  ngOnInit(): void {
  }

  openTemp(cityData){
    this.router.navigate(['/temp/'+cityData.city, { cityData : cityData.value }]);
  }

}
