import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemperatureComponent } from './temperature/temperature.component';
import { HomeComponent } from './home/home.component';

const root: Routes = [
  {
    path: "", component: HomeComponent
  },
  {
    path: "temp/:value", component: TemperatureComponent
  }
]


@NgModule({
  imports: [RouterModule.forRoot(root)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
