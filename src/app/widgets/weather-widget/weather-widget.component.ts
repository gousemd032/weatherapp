import { WIDGET } from './../widget.token';
import { Component, Input, OnInit } from '@angular/core';
import { Widget } from '../widget.interface';
import { WeatherService } from '../../weather.service';

@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.css'],
  providers: [
    {
      provide: WIDGET,
      useExisting: WeatherWidgetComponent,
    },
  ],
})
export class WeatherWidgetComponent implements Widget, OnInit {
  loginBranch = 'main';
  @Input() cityName: string;
  @Input() endpointValue: string;
  weatherData;
  isLoading = false;

  constructor(private weatherService: WeatherService) {

  }

  ngOnInit() {
    this.weatherService.getCurrentWeather(this.endpointValue).subscribe(data => {
      this.weatherData = data;
      console.log(data);
    },
      error => {
        console.log(error);
      });
  }

  load() {
    console.log('Load data from WEATHER API... ');
  }
  refresh() {
    this.isLoading = true;
    setTimeout(() => {
      this.isLoading = false;
    }, 1000);
  }
}
